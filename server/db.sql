CREATE TABLE restaurants (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    location VARCHAR(50) NOT NULL,
    price_range INT NOT NULL check(price_range >= 1 and price_range <= 5)
);

CREATE TABLE reviews (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    restaurant_id BIGINT NOT NULL REFERENCES restaurants(id),
    name VARCHAR(50) NOT NULL,
    review TEXT NOT NULL,
    rating INT NOT NULL check(rating >= 1 and rating <= 5)
);
select *
from restaurants
    left join(
        select restaurant_id,
            count(*),
            trunc(avg(rating), 1) as average_rating
        from reviews
        group by restaurant_id
    ) reviews on restaurants.id = reviews.restaurant_id;

SELECT restaurant_id, TRUNC(AVG(rating), 2) as avg_rating, COUNT(rating) as total_reviews FROM reviews GROUP BY restaurant_id
INSERT INTO restaurants (name, location, price_range) VALUES('Red rum', 'Stockholm', 3);
INSERT INTO reviews (restaurant_id, name, review, rating) VALUES(3, 'Danny', 'safakfd askdfj wkaej rlkja fak', 3);
