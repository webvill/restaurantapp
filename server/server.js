require("dotenv").config();
const morgan = require("morgan");
const cors = require("cors");
const express = require("express");
const db = require("./db");
const app = express();

app.use(cors());
app.use(express.json());

app.get("/", (req, res) => {
  res.json({ data: "hello world" });
});

app.get("/api/v1/restaurants", async (req, res) => {
  try {
    //const results = await db.query("select * from restaurants");
    const results = await db.query(`select *
    from restaurants
        left join(
            select restaurant_id,
                count(*),
                trunc(avg(rating), 1) as average_rating
            from reviews
            group by restaurant_id
        ) reviews on restaurants.id = reviews.restaurant_id`);

    res.status(200).json({
      status: "success",
      results: results.rows.length,
      data: results.rows,
    });
  } catch (error) {
    console.log(error);
  }
});

app.get("/api/v1/restaurants/:id", async (req, res) => {
  try {
    const restaurant = await db.query(
      `select *
      from restaurants
          left join(
              select restaurant_id,
                  count(*) as reviews_count,
                  trunc(avg(rating), 1) as average_rating
              from reviews
              group by restaurant_id
          ) reviews on restaurants.id = reviews.restaurant_id where id = $1`,
      [req.params.id]
    );
    const reviews = await db.query(
      "select * from reviews where restaurant_id = $1",
      [req.params.id]
    );
    res.json({
      status: "success",
      data: { restaurant: restaurant.rows[0], reviews: reviews.rows },
    });
  } catch (error) {}
});

app.post("/api/v1/restaurants", async (req, res) => {
  try {
    const { rows } = await db.query(
      "INSERT INTO restaurants (name, location, price_range) values ($1, $2, $3) returning *",
      [req.body.name, req.body.location, req.body.price_range]
    );
    res.status(201).json({ status: "success", data: rows[0] });
  } catch (error) {
    console.log(error);
  }
});

app.put("/api/v1/restaurants/:id", async (req, res) => {
  try {
    const result = await db.query(
      "UPDATE restaurants SET name=$1, location=$2, price_range=$3 where id = $4 returning *",
      [req.body.name, req.body.location, req.body.price_range, req.params.id]
    );
    res.json({ status: "success" });
  } catch (error) {
    console.log(error);
  }
});
app.delete("/api/v1/restaurants/:id", async (req, res) => {
  try {
    await db.query("DELETE FROM restaurants where id=$1", [req.params.id]);
    res.status(204).json({ data: "delete" });
  } catch (error) {
    console.log(error);
  }
});

app.post("/api/v1/restaurants/:id/reviews", async (req, res) => {
  try {
    const { rows } = await db.query(
      "INSERT INTO reviews (name, restaurant_id, rating, review) values ($1, $2, $3, $4) returning *",
      [req.body.name, req.params.id, req.body.rating, req.body.review]
    );
    res.status(201).json({ status: "success", data: rows[0] });
  } catch (error) {
    console.log(error);
  }
});

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`server is running on port ${port}`));
