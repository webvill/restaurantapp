import React, { useState, useContext } from "react";
import { useParams } from "react-router-dom";
import Yelp from "../apis/yelp";
import validate from "./validations/validateReviewData";
import { RestaurantContext } from "../context/RestaurantContext";
const AddReview = ({ setShowForm }) => {
   const { selectedRestaurant, setSelectedRestaurant } =
      useContext(RestaurantContext);

   const [errors, setErrors] = useState({});
   const [isSubmitting, setIsSubmitting] = useState({});

   const [review, setReview] = useState({
      name: "",
      review: "",
      rating: "",
   });

   console.log("selected rest", selectedRestaurant);

   const { id } = useParams();
   const handleChange = (e) => {
      const { name, value } = e.target;
      setReview({ ...review, [name]: value });
      setReview((state) => {
         isSubmitting && setErrors(validate(state));
         return state;
      });
   };

   const handleSubmit = async (e) => {
      e.preventDefault();
      setErrors(validate(review));
      setIsSubmitting(true);
      if (!Object.entries(errors).length > 0) {
         await Yelp.post(`/${id}/reviews`, review)
            .then((resp) => {
               setSelectedRestaurant({
                  ...selectedRestaurant,
                  reviews: selectedRestaurant.reviews.concat(resp.data.data),
                  average_rating:
                     (parseInt(selectedRestaurant.reviews_count) *
                        parseInt(selectedRestaurant.average_rating) +
                        parseInt(review.rating)) /
                     (parseInt(selectedRestaurant.reviews_count) + 1),
               });
               console.log(selectedRestaurant);
               setReview({ name: "", review: "", rating: "Välj rating" });
               setShowForm(false);
            })
            .catch((err) => console.log(err));
      }
   };

   return (
      <>
         <div className="row mb-3">
            <div className="col-md-8">
               <label className="form-label">Ditt namn</label>
               <input
                  type="text"
                  name="name"
                  value={review.name}
                  onChange={handleChange}
                  className={
                     errors.name ? "is-invalid form-control" : "form-control"
                  }
               />
               {errors.name && (
                  <div className="invalid-feedback">{errors.name}</div>
               )}
            </div>
            <div className="col-md-4">
               <label className="form-label">Välj rating</label>
               <select
                  onChange={handleChange}
                  name="rating"
                  value={review.rating}
                  className={
                     errors.rating
                        ? "is-invalid form-select mr-1 mr-sm-2"
                        : "form-select mr-1 mr-sm-2"
                  }
                  id=""
               >
                  <option disabled>Välj rating</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
               </select>
               {errors.rating && (
                  <div className="invalid-feedback">{errors.rating}</div>
               )}
            </div>
         </div>
         <div className="col-12 mb-3">
            <label className="form-label">Din recension</label>
            <textarea
               name="review"
               onChange={handleChange}
               className={
                  errors.review ? "is-invalid form-control" : "form-control"
               }
            />
            {errors.review && (
               <div className="invalid-feedback">{errors.review}</div>
            )}
         </div>
         <div className="col-12 text-end">
            <button
               onClick={handleSubmit}
               type="submit"
               className="btn btn-outline-secondary"
            >
               Lägg till recension
            </button>
         </div>
      </>
   );
};

export default AddReview;
