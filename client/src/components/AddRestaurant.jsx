import React from "react";
import useForm from "./useForm";
import validate from "./validations/validateRestaurantData";
const AddRestaurant = () => {
   const { handleChange, values, handleSubmit, errors } = useForm(
      validate,
      "post"
   );
   return (
      <form onSubmit={handleSubmit}>
         <div className="mb-4">
            <div className="row">
               <div className="col">
                  <input
                     type="text"
                     onChange={handleChange}
                     name="name"
                     value={values.name}
                     className={
                        errors.name ? "is-invalid form-control" : "form-control"
                     }
                     placeholder="Namm"
                  />
                  {errors.name && (
                     <div className="invalid-feedback">{errors.name}</div>
                  )}
               </div>
               <div className="col">
                  <input
                     type="text"
                     onChange={handleChange}
                     name="location"
                     value={values.location}
                     className={
                        errors.location
                           ? "is-invalid form-control"
                           : "form-control"
                     }
                     placeholder="Plats"
                  />
                  {errors.location && (
                     <div className="invalid-feedback">{errors.location}</div>
                  )}
               </div>
               <div className="col">
                  <select
                     onChange={handleChange}
                     name="price_range"
                     value={values.price_range}
                     defaultValue="Prisläge"
                     className={
                        errors.price_range
                           ? "is-invalid form-select mr-1 mr-sm-2"
                           : "form-select mr-1 mr-sm-2"
                     }
                     id=""
                  >
                     <option disabled>Prisläge</option>
                     <option value="1">$</option>
                     <option value="2">$$</option>
                     <option value="3">$$$</option>
                     <option value="4">$$$$</option>
                     <option value="5">$$$$$</option>
                  </select>
                  {errors.price_range && (
                     <div className="invalid-feedback">
                        {errors.price_range}
                     </div>
                  )}
               </div>
               <div className="col d-grid">
                  <button className="btn btn-outline-success">Lägg till</button>
               </div>
            </div>
         </div>
      </form>
   );
};

export default AddRestaurant;
