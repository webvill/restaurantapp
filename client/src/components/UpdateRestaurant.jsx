import React, { /* useContext, */ useEffect } from "react";
import { useParams } from "react-router";

import Yelp from "../apis/yelp";
import useForm from "./useForm";
import validate from "./validations/validateRestaurantData";

const UpdateRestaurant = (props) => {
   const { id } = useParams();
   const { handleChange, values, setValues, handleSubmit, errors } = useForm(
      validate,
      "put",
      id
   );

   useEffect(() => {
      const fetchData = async () => {
         await Yelp.get(`/${id}`)
            .then(({ data }) => setValues(data.data.restaurant))
            .catch((err) => console.log(err));
      };
      fetchData();
   }, []);

   return (
      <>
         <form>
            <div className="mb-4">
               <label className="form-label" htmlFor="">
                  Name
               </label>
               <input
                  type="text"
                  onChange={handleChange}
                  name="name"
                  value={values.name}
                  className="form-control"
                  placeholder="Name"
               />
            </div>
            <div className="mb-4">
               <label className="form-label" htmlFor="">
                  Location
               </label>
               <input
                  type="text"
                  onChange={handleChange}
                  name="location"
                  value={values.location}
                  className="form-control"
                  placeholder="Location"
               />
            </div>
            <div className="mb-4">
               <label className="form-label" htmlFor="">
                  Price range
               </label>
               <select
                  onChange={handleChange}
                  name="price_range"
                  value={values.price_range}
                  className="form-select mr-1 mr-sm-2"
                  id=""
               >
                  <option disabled>Price range</option>
                  <option value="1">$</option>
                  <option value="2">$$</option>
                  <option value="3">$$$</option>
                  <option value="4">$$$$</option>
                  <option value="5">$$$$$</option>
               </select>
            </div>
            <div className="form-group">
               <button onClick={handleSubmit} className="btn btn-success">
                  Update
               </button>
            </div>
         </form>
      </>
   );
};

export default UpdateRestaurant;
