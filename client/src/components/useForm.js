// Custom Hook som anropas av
import { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import Yelp from "../apis/yelp";
import { RestaurantContext } from "../context/RestaurantContext";

export const useForm = (validate, method, id) => {
   const { addRestaurant } = useContext(RestaurantContext);
   const [values, setValues] = useState({
      name: "",
      location: "",
      price_range: "",
   });
   const [errors, setErrors] = useState({});
   const [isSubmitting, setIsSubmitting] = useState(false);
   let history = useHistory();
   const handleChange = (e) => {
      const { name, value } = e.target;
      setValues({ ...values, [name]: value });
      setValues((state) => {
         isSubmitting && setErrors(validate(state));
         return state;
      });
   };
   /* const updateState = (name, value) => {
    return values;
  } */
   const handleSubmit = async (e) => {
      e.preventDefault();
      setErrors(validate(values));
      setIsSubmitting(true);
      if (!Object.entries(errors).length > 0) {
         if (method === "post") {
            await Yelp.post("/", values)
               .then((resp) => {
                  addRestaurant(resp.data.data);
                  console.log(resp);
               })
               .catch((err) => console.log(err));
         } else {
            await Yelp.put(`/${id} `, values)
               .then((resp) => {
                  console.log(resp);
                  history.push("/");
               })
               .catch((err) => console.log(err));
         }
      }
   };

   return { handleChange, values, setValues, handleSubmit, errors };
};

export default useForm;
