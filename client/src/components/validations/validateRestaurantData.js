export default function validateRestaurantData(values) {
   let errors = {};
   console.log("values from validate", values);
   if (!values.name.trim()) {
      errors.name = "Namn är required";
   }
   if (!values.location.trim()) {
      errors.location = "Plats är required";
   }
   if (!values.price_range.trim()) {
      errors.price_range = "Prisnivå är required";
   }
   console.log("errors from validate", errors);
   return errors;
}
