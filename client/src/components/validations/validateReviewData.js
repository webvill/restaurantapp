export default function validateReviewData(values) {
   let errors = {};
   console.log("values from validate", values);
   if (!values.name.trim()) {
      errors.name = "Namn är required";
   }
   if (!values.rating.trim()) {
      errors.rating = "Rating är required";
   }
   if (!values.review.trim()) {
      errors.review = "Recencion är required";
   }
   console.log("errors from validate", errors);
   return errors;
}
