import React from "react";
import Rating from "./Rating";

const Review = ({ reviews, ascending }) => {
   const sortFunction = () => {
      if (!ascending) {
         return (a, b) => b.rating - a.rating;
      }
      return (a, b) => a.rating - b.rating;
   };
   return (
      <div className="row row-cols-3 mb-4">
         {reviews &&
            reviews.sort(sortFunction()).map((review) => {
               return (
                  <div
                     key={review.id}
                     className="card text-white bg-primary m-3"
                     style={{ maxWidth: "30%" }}
                  >
                     <div className="card-header d-flex justify-content-between">
                        <span>{review.name}</span>
                        <span>
                           <Rating rating={review.rating} />
                        </span>
                     </div>
                     <div className="card-body">
                        <p className="card-text">{review.review}</p>
                     </div>
                  </div>
               );
            })}
      </div>
   );
};

export default Review;
