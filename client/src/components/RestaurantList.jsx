import React, { useContext, useEffect } from "react";
import { useHistory } from "react-router";
import Yelp from "../apis/yelp";
import { RestaurantContext } from "../context/RestaurantContext";
import Rating from "./Rating";
const RestaurantList = (props) => {
   const { restaurants, setRestaurants, setSelectedRestaurant } =
      useContext(RestaurantContext);
   let history = useHistory();

   useEffect(() => {
      const fetchData = async () => {
         await Yelp.get("/")
            .then((resp) => setRestaurants(resp.data.data))
            .catch((err) => console.log(err));
      };
      fetchData();
   }, [setRestaurants]);
   console.log("restaurants", restaurants);

   const handleDelete = async (e, id) => {
      e.stopPropagation();
      await Yelp.delete(`/${id}`)
         .then((resp) => {
            setRestaurants(restaurants.filter((r) => r.id !== id));
         })
         .catch((err) => console.log(err));
   };

   const handleUpdate = (e, id) => {
      e.stopPropagation();
      history.push(`/restaurants/${id}/update`);
   };

   const handleSelectedRestaurant = (id) => {
      history.push(`/restaurants/${id}`);
   };

   return (
      <table className="table table-dark table-hover">
         <thead>
            <tr className="table-primary text-white">
               <th>Restaurant</th>
               <th>Location</th>
               <th>Price range</th>
               <th>Ratings</th>
               <th>Update/Delete</th>
            </tr>
         </thead>
         <tbody>
            {restaurants &&
               restaurants.map((r) => {
                  return (
                     <tr
                        key={r.id}
                        onClick={() => handleSelectedRestaurant(r.id)}
                     >
                        <td>{r.name}</td>
                        <td>{r.location}</td>
                        <td>{"$".repeat(r.price_range)}</td>
                        <td>
                           {r.count ? (
                              <>
                                 <Rating rating={r.average_rating} />{" "}
                                 <span className="text-warning ml-2">
                                    ({r.count})
                                 </span>
                              </>
                           ) : (
                              <span className="text-warning">No reviews</span>
                           )}
                        </td>
                        <td>
                           <i
                              onClick={(e) => handleDelete(e, r.id)}
                              class="bi bi-trash2-fill me´2"
                           ></i>
                           <i
                              onClick={(e) => handleUpdate(e, r.id)}
                              class="bi bi-pencil-square"
                           ></i>
                        </td>
                     </tr>
                  );
               })}
         </tbody>
      </table>
   );
};

export default RestaurantList;
