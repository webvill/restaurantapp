import React, { useState } from "react";
import AddRestaurant from "../components/AddRestaurant";
import Header from "../components/Header";
import RestaurantList from "../components/RestaurantList";

const Home = () => {
   const [showForm, setShowForm] = useState(false);
   return (
      <>
         <Header />
         <div className="container mt-3">
            <button
               onClick={() => setShowForm(!showForm)}
               className="btn btn-outline-secondary mb-3"
            >
               {showForm ? "Hide form" : "Add restaurant"}
            </button>
            {showForm && <AddRestaurant />}
            <RestaurantList />
         </div>
      </>
   );
};

export default Home;
