import React, { useEffect, useContext, useState } from "react";
import { useParams } from "react-router-dom";
import Yelp from "../apis/yelp";
import AddReview from "../components/AddReview";
import Header from "../components/Header";
import Rating from "../components/Rating";
import Review from "../components/Review";
import { RestaurantContext } from "../context/RestaurantContext";

const RestaurantDetailPage = () => {
   const [showForm, setShowForm] = useState(false);
   const [ascending, setAscending] = useState(false);
   const { selectedRestaurant, setSelectedRestaurant } =
      useContext(RestaurantContext);
   const { id } = useParams();

   // SET SELECTED RESTAURANT
   useEffect(() => {
      const fetchData = async () => {
         await Yelp.get(`/${id}`)
            .then((resp) => {
               setSelectedRestaurant(resp.data.data);
            })
            .catch((err) => console.log(err));
      };
      fetchData();
   }, []);

   return (
      <>
         <Header />
         {selectedRestaurant && (
            <>
               <h1 className="text-center display-1">
                  {selectedRestaurant.restaurant.name}
               </h1>
               {selectedRestaurant.restaurant.reviews_count && (
                  <>
                     <div className="text-center">
                        <Rating
                           rating={selectedRestaurant.restaurant.average_rating}
                        />
                        <span className="text-warning ml-2">
                           ({selectedRestaurant.restaurant.average_rating})
                        </span>
                     </div>
                  </>
               )}
               <div className="d-flex justify-content-around">
                  <button
                     type="button"
                     onClick={() => setShowForm(!showForm)}
                     className="btn btn-outline-secondary mt-3"
                  >
                     {showForm ? "Dölj formulär" : "Lägg till review"}
                  </button>
                  <button
                     type="button"
                     onClick={() => setAscending(!ascending)}
                     className="btn btn-outline-info mt-3"
                  >
                     {ascending ? (
                        <i class="bi bi-arrow-up">Visa fallande</i>
                     ) : (
                        <i class="bi bi-arrow-down">Visa stigande</i>
                     )}
                  </button>
               </div>
               <div className="mt-3">
                  {showForm && (
                     <div className="card">
                        <div className="card-body">
                           <AddReview setShowForm={setShowForm} />
                        </div>
                     </div>
                  )}
               </div>

               <Review
                  ascending={ascending}
                  reviews={selectedRestaurant.reviews}
               />
            </>
         )}
      </>
   );
};

export default RestaurantDetailPage;
