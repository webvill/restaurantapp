import React from "react";
import Header from "../components/Header";
import UpdateRestaurant from "../components/UpdateRestaurant";

const UpdatePage = () => {
   return (
      <div className="container">
         <Header />
         <h1>Update restaurant</h1>
         <UpdateRestaurant />
      </div>
   );
};

export default UpdatePage;
